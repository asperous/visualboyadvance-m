#include "zremote.h"
#include "zremote_util.h"


Remote::Remote()
        : context(1), publisher(context, ZMQ_PUB) {
    publisher.connect("tcp://pkmn.snc.io:5555");
}

unsigned char lastX = 0;
unsigned char lastY = 0;
unsigned char lastD = 0;
u32 lastM = 0;
unsigned char deaths = 0;
bool isCurrentlyDead = false;
bool isReady = false;

struct MsgStruct {
    unsigned char x;
    unsigned char y;
    unsigned char direction;
    unsigned char numberCaught;
    unsigned char map_bank;
    unsigned char map_number;
    unsigned char deaths;
    u32 money;
    u32 name1;
    u32 name2;
};

struct MsgStruct msgStruct;

void Remote::sendUpdate(bool forceSend) {
    u8 width = zRemote::mapWidth();
    u8 height = zRemote::mapHeight();
    if (width && height) {
        msgStruct.x = (unsigned char) (100 * zRemote::spriteX() / width);
        msgStruct.y = (unsigned char) (100 * zRemote::spriteY() / height);
        msgStruct.direction = (unsigned char) zRemote::spriteDirection();
        unsigned char isDead = (unsigned char) zRemote::isDead();

        if (isReady && isDead == 1 && !isCurrentlyDead) {
            isCurrentlyDead = true;
            deaths++;
        } else if (isDead == 0 && isCurrentlyDead) {
            isCurrentlyDead = false;
        }
        msgStruct.deaths = deaths;

        if (msgStruct.x != lastX
            || msgStruct.y != lastY
            || msgStruct.direction != lastD
            || msgStruct.money != lastM
            || forceSend) {
            msgStruct.numberCaught = zRemote::getPokemonCaughtCount();
            msgStruct.map_bank = (unsigned char) zRemote::mapBank();
            msgStruct.map_number = (unsigned char) zRemote::mapNumber();
            msgStruct.name1 = zRemote::spriteName1();
            msgStruct.name2 = zRemote::spriteName2();
            msgStruct.money = zRemote::readMoney();

            if (!isReady && msgStruct.y != 0 && msgStruct.x != 0 && msgStruct.direction != 0)
                isReady = true;
            else if (!isReady)
                return;

            lastD = msgStruct.direction;
            lastX = msgStruct.x;
            lastY = msgStruct.y;
            lastM = msgStruct.money;

            publisher.send(&msgStruct, sizeof(struct MsgStruct), 0);
        }
    }
}
