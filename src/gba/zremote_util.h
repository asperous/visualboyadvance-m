#include "Globals.h"
#include "GBAinline.h"

namespace zRemote {
    u32 readMoney() {
        return CPUReadMemory(CPUReadMemory(0x0300500C) + 0x0F20) ^ CPUReadMemory(CPUReadMemory(0x03005008) + 0x0290);
    }

    u8 mapWidth() {
        return CPUReadByte(CPUReadMemory(0x02036DFC));
    }

    u8 mapHeight() {
        return CPUReadByte(CPUReadMemory(0x02036DFC) + 0x04);
    }

    u8 mapBank() {
        return CPUReadByte(0x02031DBC);
    }

    u8 mapNumber() {
        return CPUReadByte(0x02031DBD);
    }

    u8 spriteX() {
        return CPUReadMemory(CPUReadMemory(0x03005008));
    }

    u8 spriteY() {
        return CPUReadMemory(CPUReadMemory(0x03005008) + 0x02);
    }

    u8 spriteDirection() {
        return CPUReadByte(0x2020666) & 0b0011; // Mask out all but the last two bits;
    }

    u8 spriteState() {
        return CPUReadByte(0x02036E58);
    }

    u32 spriteName1() {
        return CPUReadMemory(CPUReadMemory(0x0300500C));
    }

    u32 spriteName2() {
        return CPUReadMemory(CPUReadMemory(0x0300500C) + 0x04);
    }

    int BitCount(unsigned int u) {
        unsigned int uCount;

        uCount = u - ((u >> 1) & 033333333333) - ((u >> 2) & 011111111111);
        return ((uCount + (uCount >> 3)) & 030707070707) % 63;
    }

    u8 getPokemonCaughtCount() {
        int base = CPUReadMemory(0x0300500C);
        int total = 0;
        for (int offset = 0x028; offset < 0x05c; offset += 4) {
            total += BitCount(CPUReadMemory(base + offset));
        }
        total += BitCount(CPUReadMemory(base + 0x028) & 0xFFF00000);
        return total;
    }

    u8 isDead() {
        int total_pokemon = 0;
        int number_dead = 0;
        for (int pokemon_index = 0; pokemon_index < 6; pokemon_index++) {
            int current_health = CPUReadHalfWord(0x02024284 + (pokemon_index * 100) + 86);
            int total_health = CPUReadHalfWord(0x02024284 + (pokemon_index * 100) + 88);
            if (total_health > 0)
                total_pokemon++;
            if(total_health > 0 && current_health == 0)
                number_dead++;
        }
        if (total_pokemon > 1 && number_dead == total_pokemon)
            return 1;
        else
            return 0;
    }
}